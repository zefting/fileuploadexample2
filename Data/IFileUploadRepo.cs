using FileUoloadExample2.Models;

namespace FileUoloadExample2.Data
{
    public interface IFileUploadRepo
    {
        // Beers GetBeerById(int Beer_id);
        FileUpload GetFileByName(string getFileName);
        bool SaveChanges();
        void SaveFileName(FileUpload fileUpload);
    }
}