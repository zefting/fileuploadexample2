using FileUoloadExample2.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Linq;

namespace FileUoloadExample2.Data
{
    public class FileUoloadExample2RepoImpli :IFileUploadRepo
    {
        private readonly FileUploadContext _context;
        public FileUoloadExample2RepoImpli(FileUploadContext context)
        {
            _context = context;
        }
        public bool SaveChanges()
        {
            return(_context.SaveChanges() >=0);
        }
        public void SaveFileName(FileUpload fileUpload)
        {
            if(fileUpload == null)
            {
                throw new ArgumentNullException(nameof(fileUpload));
            }
            _context.FileUpload.Add(fileUpload);
        }
        public FileUpload GetFileByName(string getFileName)
        {
            return _context.FileUpload.FirstOrDefault(p => p.NewFileName == getFileName);
        }
    }
}