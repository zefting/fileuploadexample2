﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace FileUoloadExample2.Models
{
    public partial class FileUploadContext : DbContext
    {
        public FileUploadContext()
        {
        }

        public FileUploadContext(DbContextOptions<FileUploadContext> options)
            : base(options)
        {
        }

        public virtual DbSet<FileUpload> FileUpload { get; set; }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FileUpload>(entity =>
            {
                entity.ToTable("FileUpload", "fileupload");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.NewFileName)
                    .IsRequired()
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.OldFileName)
                    .IsRequired()
                    .HasMaxLength(1000)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
