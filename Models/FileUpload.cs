﻿using System;
using System.Collections.Generic;

namespace FileUoloadExample2.Models
{
    public partial class FileUpload
    {
        public int Id { get; set; }
        public string OldFileName { get; set; }
        public string NewFileName { get; set; }
    }
}
