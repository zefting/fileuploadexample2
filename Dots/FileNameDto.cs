using System;
using System.Collections.Generic;

namespace FileUoloadExample2.Dtos
{
    public class FileUploadDto
    {
        public int Id { get; set; }
        public string OldFileName { get; set; }
        public string NewFileName { get; set; }
    }
}

