using System;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using FileUoloadExample2.Data;
using FileUoloadExample2.Models;
using AutoMapper;
using FileUoloadExample2.Dtos;
using Microsoft.AspNetCore.Http;

namespace FileUoloadExample2.Controllers
{
    [Route("api")]
    [ApiController]
    public class UploadFileController : ControllerBase
    {
        private readonly IFileUploadRepo _repository;
        private readonly IMapper _mapper;
        public UploadFileController(IFileUploadRepo repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        [Route("UploadFile")]
        [HttpPost, DisableRequestSizeLimit]
        public IActionResult UploadFile()
        {
            try
            {
                string jpgType = ".jpg";
                string pgnType = ".pgn";
                var file = Request.Form.Files[0];
                string tempfolderName = Path.Combine("StaticFiles", "Uploads", "temp");
                string folderName = Path.Combine("StaticFiles", "Uploads");
                string jpgFolderName = Path.Combine("StaticFiles", "Uploads", "jpg");
                string pgnFolderName = Path.Combine("StaticFiles", "Uploads", "pgn");
                string tempPathToSave = Path.Combine(Directory.GetCurrentDirectory(), tempfolderName);
                if (file.Length > 0)
                {
                    string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim().ToString();
                    // der skal nok laves en string som holder den her sammen med stien?
                    var newFileName = Guid.NewGuid();

                    string tempfullPath = Path.Combine(tempfolderName, fileName);
                    //Den her skal laves rigtigt.
                    string dbPath = Path.Combine(Directory.GetCurrentDirectory(), newFileName.ToString());
                    using (var stream = new FileStream(tempfullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                    // check if the file was created, and if so get the file type and move it to the right folder.
                    if (System.IO.File.Exists(tempfullPath))
                    {
                        // get filetype
                        var fileType = Path.GetExtension(tempfullPath);
                        // if jpg, move it to the jpg folder, and if succes, delete the temp file.
                        if (fileType == jpgType)
                        {
                            // skal den her i en using?
                            System.IO.File.Move(tempfullPath, Path.Combine(jpgFolderName, newFileName + jpgType));
                            if (System.IO.File.Exists(Path.Combine(jpgFolderName, newFileName + jpgType)))
                            {
                                var saveName = new FileUpload();
                                saveName.OldFileName = fileName;
                                saveName.NewFileName = Path.Combine(newFileName + jpgType);
                                _repository.SaveFileName(saveName);
                                _repository.SaveChanges();
                            }

                        }
                        // if pgn, move it to the jpg folder, and if succes, delete the temp file.
                        if (fileType == pgnType)
                        {

                            System.IO.File.Move(tempfullPath, Path.Combine(jpgFolderName, newFileName + pgnType));
                            if (System.IO.File.Exists(Path.Combine(pgnFolderName, newFileName + pgnType)))
                            {
                                var saveName = new FileUpload();
                                saveName.OldFileName = fileName;
                                saveName.NewFileName = Path.Combine(pgnFolderName, newFileName + pgnType);
                                _repository.SaveFileName(saveName);
                                _repository.SaveChanges();
                            }

                        }
                    }
                    // return the path saved to front
                    //Det her skal fixes, så den henter det nye object
                    return Ok(new { dbPath });
                    //  SaveFileName(FileUpload fileUpload)
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"internal server error: {ex}");
            }
        }

        [HttpPost]
        [Route("getFile")]
        public FileStreamResult GetFileByName(FileUploadJsonDto getFileName)
        {
            // Get the upload basefolder. If this example no user is given, but here a user could be specificed a that users folder found. 
            string folderName = Path.Combine("StaticFiles", "Uploads");
            // Fetch the new filename from the database and populate it to the FileUpload model.
            var fileItem = _repository.GetFileByName(getFileName.NewFileName);
            // get the selected file's type and remove the dot.
            var fileType = Path.GetExtension(fileItem.NewFileName).Replace(".", "");
            // search the directories (and sub folders - SearchOption.AllDirectories) specified in folderName.
            string[] filePath = Directory.GetFiles(folderName, fileItem.NewFileName, SearchOption.AllDirectories);
            // Since each filename is uniq, the first item in the filePath array will always be the selected file. 
            string newFileNameFolderpath = filePath[0];
            // open a filesteam 
            var stream = new FileStream(newFileNameFolderpath, System.IO.FileMode.Open);
            // return the file to the user and set contenttype to image/ + fileType
            return new FileStreamResult(stream, "image/" + fileType)
            {
                // rename the file to the users original filename 
                FileDownloadName = fileItem.OldFileName
            };
        }
    }
}