using AutoMapper;
using FileUoloadExample2.Models;
using FileUoloadExample2.Dtos;

namespace FileUoloadExample2.Profiles
{
    public class FileUploadsProfile : Profile
    {
        public FileUploadsProfile()
        {
            CreateMap<FileUpload, FileUploadDto>();
            CreateMap<FileUploadDto, FileUpload>();
        }
    }
}